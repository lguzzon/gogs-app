#!/bin/bash

set -eu -o pipefail

mkdir -p /run/gogs/tmp/uploads

setup_ldap_source() {
    set -eu

    # Wait for gogs to finish db setup, before we insert ldap source in db
    while ! curl --fail http://localhost:3000/healthcheck; do
        echo "Waiting for gogs to come up"
        sleep 1
    done

    now=$(date +%s)

    # Get the existing LDAP source status. This allows the user to disable LDAP
    ldap_status=$(mysql -u"${MYSQL_USERNAME}" -p"${MYSQL_PASSWORD}" -h mysql --database="${MYSQL_DATABASE}" -N -B -e "select is_actived from login_source WHERE name='cloudron';")
    [[ -z "${ldap_status}" ]] && ldap_status="1"

    if mysql -u"${MYSQL_USERNAME}" -p"${MYSQL_PASSWORD}" -h mysql --database="${MYSQL_DATABASE}" \
        -e "REPLACE INTO login_source (id, type, name, is_actived, cfg, created_unix, updated_unix) VALUES (1,2,'cloudron',${ldap_status},'{\"Name\":\"cloudron\",\"Host\":\"${LDAP_SERVER}\",\"Port\":${LDAP_PORT},\"UseSSL\":false,\"SkipVerify\":true,\"BindDN\":\"${LDAP_BIND_DN}\",\"BindPassword\":\"${LDAP_BIND_PASSWORD}\",\"UserBase\":\"${LDAP_USERS_BASE_DN}\",\"AttributeUsername\":\"username\",\"AttributeName\":\"displayname\",\"AttributeSurname\":\"\",\"AttributeMail\":\"mail\",\"Filter\":\"(\\\\u007C(mail=%[1]s)(username=%[1]s))\",\"AdminFilter\":\"(memberof=cn=admins,${LDAP_GROUPS_BASE_DN})\"}','${now}','${now}');"; then
        echo "LDAP Authentication was setup with status ${ldap_status}"
    else
        echo "Failed to setup LDAP authentication"
        exit 1
    fi
}

# SSH_PORT can be unset to disable SSH
disable_ssh="false"
if [[ -z "${SSH_PORT:-}" ]]; then
    echo "SSH disabled"
    SSH_PORT=29418 # arbitrary port to keep sshd happy
    disable_ssh="true"
fi

if [[ ! -f "/app/data/sshd/ssh_host_ed25519_key" ]]; then
    echo "Generating ssh host keys"
    mkdir -p /app/data/sshd
    ssh-keygen -qt rsa -N '' -f /app/data/sshd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sshd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sshd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sshd/ssh_host_ed25519_key
else
    echo "Reusing existing host keys"
fi

chmod 0600 /app/data/sshd/*_key
chmod 0644 /app/data/sshd/*.pub

sed -e "s/^Port .*/Port ${SSH_PORT}/" \
    -e "s/^#ListenAddress .*/ListenAddress 0.0.0.0/" \
    -e "s,^HostKey /etc/ssh/,HostKey /app/data/sshd/," \
    /etc/ssh/sshd_config > /run/gogs/sshd_config

sed -e "s/##DOMAIN/${APP_DOMAIN}/g" \
    -e "s/##SSH_PORT/${SSH_PORT}/g" \
    -e "s/##DISABLE_SSH/${disable_ssh}/g" \
    -e "s/##MYSQL_HOST/${MYSQL_HOST}/g" \
    -e "s/##MYSQL_PORT/${MYSQL_PORT}/g" \
    -e "s/##MYSQL_USERNAME/${MYSQL_USERNAME}/g" \
    -e "s/##MYSQL_PASSWORD/${MYSQL_PASSWORD}/g" \
    -e "s/##MYSQL_DATABASE/${MYSQL_DATABASE}/g" \
    -e "s/##MAIL_SERVER/${MAIL_SMTP_SERVER}/g" \
    -e "s/##MAIL_PORT/${MAIL_SMTP_PORT}/g" \
    -e "s/##MAIL_FROM/${MAIL_FROM}/g" \
    -e "s/##MAIL_SMTP_USERNAME/${MAIL_SMTP_USERNAME}/g" \
    -e "s/##MAIL_SMTP_PASSWORD/${MAIL_SMTP_PASSWORD}/g" \
    -e "s/##SECRET_KEY/$(pwgen -1 -s)/g" \
    /home/git/app.ini.template > "/run/gogs/app.ini"

# merge any user config file
[[ -f /app/data/app.ini ]] && cat "/app/data/app.ini" >> "/run/gogs/app.ini"

mkdir -p /app/data/repository /app/data/ssh

chown -R git:git /app/data /run/gogs

( setup_ldap_source ) &

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Gogs

