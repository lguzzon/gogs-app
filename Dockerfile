FROM cloudron/base:0.10.0

RUN apt-get update && \
    apt-get install -y openssh-server && \
    rm -rf /etc/ssh_host_* && \
    rm -r /var/cache/apt /var/lib/apt/lists

ADD supervisor/ /etc/supervisor/conf.d/

RUN adduser --disabled-login --gecos 'Gogs' git
# by default, git account is created as inactive which prevents login via openssh
# https://github.com/gitlabhq/gitlabhq/issues/5304
RUN passwd -d git

RUN mkdir -p /home/git/gogs
## TODO: use redis as well
RUN cd /home/git/gogs && \
    curl -L $(curl -k -s https://api.github.com/repos/gogits/gogs/releases | grep browser_download_url  | grep linux_amd64.tar.gz | head -n1 | cut -d '"' -f 4) | tar zxvf - --strip-components 1

# setup config paths
ADD app.ini.template /home/git/app.ini.template

# setup log paths
RUN mkdir -p /run/gogs && chown -R git:git /run/gogs
RUN sed -e 's,^logfile=.*$,logfile=/run/gogs/supervisord.log,' -i /etc/supervisor/supervisord.conf

RUN ln -s /app/data/ssh /home/git/.ssh
RUN ln -s /app/data/gitconfig /home/git/.gitconfig

ADD start.sh /home/git/start.sh

# disable pam authentication for sshd
RUN sed -e 's/UsePAM yes/UsePAM no/' -e 's/UsePrivilegeSeparation yes/UsePrivilegeSeparation no/' -i /etc/ssh/sshd_config
RUN echo "UseDNS no" >> /etc/ssh/sshd_config

CMD [ "/home/git/start.sh" ]

