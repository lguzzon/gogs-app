This app integrates with the Cloudron SSO. Admins on Cloudron automatically
become admins on Gogs.

If you want to disable Cloudron SSO, do the following:

* Admin Panel -> Authentication -> 'cloudron' -> Uncheck 'This authentication is activated'
* Admin Panel -> Users -> Change Authentication Source to 'Local' and also give a password

You can create a `/app/data/app.ini` with any custom configuration. See the 
[configuration cheat sheet](https://gogs.io/docs/advanced/configuration_cheat_sheet)
for more information.

