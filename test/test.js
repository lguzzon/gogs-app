#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var repodir = '/tmp/testrepo';
    var app, reponame = 'testrepo';
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = process.env.TIMEOUT || 5000;
    var email, token;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        rimraf.sync(repodir);
        done();
    });

    function waitForUrl(url, done) {
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (currentUrl) {
                return currentUrl === url;
            });
        }, TIMEOUT).then(function () { done(); });
    }

    function setAvatar(done) {
        browser.get('https://' + app.fqdn + '/user/settings/avatar');

        browser.findElement(by.xpath('//input[@type="file" and @name="avatar"]')).sendKeys(path.resolve(__dirname, '../logo.png')).then(function () {
            browser.findElement(by.xpath('//button[contains(text(), "Update Avatar Setting")]')).click();

            browser.wait(until.elementLocated(by.xpath('//p[contains(text(),"updated successfully")]')), TIMEOUT).then(function () { done(); });
        });
    }

    function checkAvatar(done) {
        superagent.get('https://' + app.fqdn + '/avatars/1').end(function (error, result) {
            expect(error).to.be(null);
            expect(result.statusCode).to.be(200);
            done();
        });
    }

    function editFile(done) {
        browser.get('https://' + app.fqdn + '/' + username + '/' + reponame + '/_edit/master/newfile');

        var cm = browser.findElement(by.xpath('//div[contains(@class,"CodeMirror")]'));
        var text = 'yo';
        browser.executeScript('arguments[0].CodeMirror.setValue("' + text + '");', cm).then(function () {
            browser.findElement(by.xpath('//input[@name="commit_summary"]')).sendKeys('Dummy edit');
            browser.findElement(by.xpath('//button[contains(text(), "Commit Changes")]')).click();

            waitForUrl('https://' + app.fqdn + '/' + username + '/' + reponame + '/src/master/newfile', done);
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: username,
            password: password
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            token = result.body.token;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                if (error) return done(error);
                if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                email = result.body.email;
                done();
            });
        });
    });


    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can login', function (done) {
        browser.get('https://' + app.fqdn + '/user/login');
        browser.findElement(by.id('user_name')).sendKeys(username);
        browser.findElement(by.id('password')).sendKeys(password);
        browser.findElement(by.tagName('form')).submit();
        browser.wait(until.elementLocated(by.linkText('Dashboard')), TIMEOUT).then(function () { done(); });
    });

    it('can set avatar', setAvatar);
    it('can get avatar', checkAvatar);

    it('can add public key', function (done) {
        browser.get('https://' + app.fqdn + '/user/settings/ssh');
        var publicKey = fs.readFileSync(__dirname + '/id_rsa.pub', 'utf8');

        browser.findElement(by.xpath('//div[text()="Add Key"]')).click();
        browser.findElement(by.id('title')).sendKeys('testkey');
        browser.findElement(by.id('content')).sendKeys(publicKey.trim()); // #3480
        browser.findElement(by.xpath('//button[contains(text(), "Add Key")]')).click();
        browser.wait(until.elementLocated(by.xpath('//p[contains(text(), "added successfully!")]')), TIMEOUT).then(function () { done(); });
    });

    it('can create repo', function (done) {
        browser.get('https://' + app.fqdn);
        browser.findElement(by.linkText('New Repository')).click();
        browser.wait(until.elementLocated(by.xpath('//*[contains(text(), "New Repository")]')), TIMEOUT);
        browser.findElement(by.id('repo_name')).sendKeys(reponame);
        browser.findElement(by.id('auto-init')).click();
        browser.findElement(by.xpath('//button[contains(text(), "Create Repository")]')).click();
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/' + username + '/' + reponame;
            });
        }, TIMEOUT).then(function () { done(); });
    });

    it('displays correct clone url', function (done) {
        browser.get('https://' + app.fqdn + '/' + username + '/' + reponame);
        browser.findElement(by.id('repo-clone-ssh')).click();
        browser.findElement(by.id('repo-clone-url')).getAttribute('value').then(function (cloneUrl) {
            expect(cloneUrl).to.be('ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git');
            done();
        });
    });

    it('can clone the url', function (done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git ' + repodir, { env: env });
        done();
    });

    it('can add and push a file', function (done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('touch newfile && git add newfile && git commit -a -mx && git push ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + ' master',
                 { env: env, cwd: repodir });
        rimraf.sync('/tmp/testrepo');
        done();
    });

    it('can edit file', editFile);

    it('can restart app', function (done) {
        execSync('cloudron restart');
        done();
    });

    it('can clone the url', function (done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git ' + repodir, { env: env });
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
        rimraf.sync(repodir);
        done();
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get avatar', checkAvatar);

    it('can clone the url', function (done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git ' + repodir, { env: env });
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
        rimraf.sync(repodir);
        done();
    });

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', function (done) {
        browser.get('https://' + app.fqdn + '/user/login');
        browser.findElement(by.id('user_name')).sendKeys(username);
        browser.findElement(by.id('password')).sendKeys(password);
        browser.findElement(by.tagName('form')).submit();
        browser.wait(until.elementLocated(by.linkText('Dashboard')), TIMEOUT).then(function () { done(); });
    });

    it('can get avatar', checkAvatar);

    it('displays correct clone url', function (done) {
        browser.get('https://' + app.fqdn + '/' + username + '/' + reponame);
        browser.findElement(by.id('repo-clone-ssh')).click();
        browser.findElement(by.id('repo-clone-url')).getAttribute('value').then(function (cloneUrl) {
            expect(cloneUrl).to.be('ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git');
            done();
        });
    });

    it('can clone the url', function (done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + username + '/' + reponame + '.git ' + repodir, { env: env });
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
        rimraf.sync(repodir);
        done();
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // check if the _first_ login via email succeeds
    it('can login via email', function (done) {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');

        browser.get('https://' + app.fqdn + '/user/login');
        browser.findElement(by.id('user_name')).sendKeys(email);
        browser.findElement(by.id('password')).sendKeys(password);
        browser.findElement(by.tagName('form')).submit();
        browser.wait(until.elementLocated(by.linkText('Dashboard')), TIMEOUT).then(function () {
            execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            done();
        });
    });
});
